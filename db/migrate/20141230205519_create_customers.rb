class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :middle_initial
      t.string :address, null: false
      t.string :city, null: false
      t.string :state, null: false
      t.string :zip_code, null: false
      t.string :phone_number, null: false
      t.string :email_address
      t.integer :contracts_count, null: false, default: 0
      t.timestamp :contracts_updated_at

      t.timestamps null: false
    end
  end
end
