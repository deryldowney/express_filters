FactoryGirl.define do
  factory :contract do
    after(:save) {|instance| self.customer.update_column(:contracts_updated_at, Time.now)}
    customer nil
  end

end
